package matrixlibrary;

import java.util.Arrays;

public class MatrixMath implements IMatrixMath {
    /**
     * Tworzy macierz odwrotną o ile to możliwe
     *
     * @param m1
     */
    public IMatrix inverseMatrix(IMatrix m1) {
        double[][] auxiliaryMatrix;
        int[] index;

        auxiliaryMatrix = new double[m1.getHeight()][m1.getHeight()];
        IMatrix invertedMatrix = new Matrix();
        index = new int[m1.getHeight()];

        for (int i = 0; i < m1.getHeight(); ++i) {
            auxiliaryMatrix[i][i] = 1;
        }

        transformToUpperTriangle(m1.getMatrixValues(), index);

        for (int i = 0; i < (m1.getHeight() - 1); ++i) {
            for (int j = (i + 1); j < m1.getHeight(); ++j) {
                for (int k = 0; k < m1.getHeight(); ++k) {
                    auxiliaryMatrix[index[j]][k] -= m1.getMatrixValue(index[j], i) * auxiliaryMatrix[index[i]][k];
                }
            }
        }

        for (int i = 0; i < m1.getHeight(); ++i) {
            invertedMatrix.setMatrixValue(m1.getHeight() - 1, i, auxiliaryMatrix[index[m1.getHeight() - 1]][i] / m1.getMatrixValue(index[m1.getHeight() - 1], m1.getHeight() - 1));

            for (int j = (m1.getHeight() - 2); j >= 0; --j) {
                invertedMatrix.setMatrixValue(j, i, auxiliaryMatrix[index[j]][i]);

                for (int k = (j + 1); k < m1.getHeight(); ++k) {
                    invertedMatrix.setMatrixValue(i, j, invertedMatrix.getMatrixValue(j, i) - (m1.getMatrixValue(index[j], k) * invertedMatrix.getMatrixValue(k, i)));
                }

                invertedMatrix.setMatrixValue(i, j, invertedMatrix.getMatrixValue(j, i) / (m1.getMatrixValue(index[j], j)));
            }
        }

        return invertedMatrix;
    }

    private static void transformToUpperTriangle(double[][] matrix, int[] index) {
        double[] c;
        double c0, c1, pi0, pi1, pj;
        int itmp, k;

        c = new double[matrix.length];

        for (int i = 0; i < matrix.length; ++i) {
            index[i] = i;
        }

        for (int i = 0; i < matrix.length; ++i) {
            c1 = 0;

            for (int j = 0; j < matrix.length; ++j) {
                c0 = Math.abs(matrix[i][j]);

                if (c0 > c1) {
                    c1 = c0;
                }
            }

            c[i] = c1;
        }

        k = 0;

        for (int j = 0; j < (matrix.length - 1); ++j) {
            pi1 = 0;

            for (int i = j; i < matrix.length; ++i) {
                pi0 = Math.abs(matrix[index[i]][j]);
                pi0 /= c[index[i]];

                if (pi0 > pi1) {
                    pi1 = pi0;
                    k = i;
                }
            }

            itmp = index[j];
            index[j] = index[k];
            index[k] = itmp;

            for (int i = (j + 1); i < matrix.length; ++i) {
                pj = matrix[index[i]][j] / matrix[index[j]][j];
                matrix[index[i]][j] = pj;

                for (int l = (j + 1); l < matrix.length; ++l) {
                    matrix[index[i]][l] -= pj * matrix[index[j]][l];
                }
            }
        }
    }

    /**
     * Dodawanie macierzy, zwracamy uwage na poprawność rozmiaru macierzy
     *
     * @param m1
     * @param m2
     */
    public IMatrix matrixAddition(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
        if (m1.getHeight() != m2.getHeight() || m1.getHeight() != m2.getHeight()) {
            throw new InvalidDimensionException("InvalidDimensionException matrixAddition");
        }

        double[][] result = new double[m1.getHeight()][m1.getWidth()];
        for (int i = 0; i < m1.getHeight(); i++) {
            for (int j = 0; j < m1.getWidth(); j++) {
                result[i][j] = m1.getMatrixValue(i, j) + m2.getMatrixValue(i, j);
            }
        }

        IMatrix matrixAddition = new Matrix();
        matrixAddition.setMatrixValues(result);

        return matrixAddition;
    }

    /**
     * Mnożenie dwóch macierzy, zwracamy uwagę na zgodność rozmiarów
     *
     * @param m1
     * @param m2
     */
    public IMatrix matrixMultiplication(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
        double[][] result;
        int xColumns, xRows, yColumns, yRows;

        xRows = m1.getHeight();
        xColumns = m1.getWidth();
        yRows = m2.getHeight();
        yColumns = m2.getWidth();
        result = new double[xRows][yColumns];

        if (xColumns != yRows) {
            throw new InvalidDimensionException("Matrices don't match");
        }


        for (int i = 0; i < xRows; i++) {
            for (int j = 0; j < yColumns; j++) {
                for (int k = 0; k < xColumns; k++) {
                    result[i][j] += (m1.getMatrixValue(i, k) * m2.getMatrixValue(k, j));
                }
            }
        }

        IMatrix matrixMultiplication = new Matrix();
        matrixMultiplication.setMatrixValues(result);
        return matrixMultiplication;
    }

    /**
     * Odejmowanie macierzy, zwracamy uwage na poprawność rozmiaru macierzy
     *
     * @param m1
     * @param m2
     */
    public IMatrix matrixSubtracting(IMatrix m1, IMatrix m2) throws InvalidDimensionException {
        if (m1.getHeight() != m2.getHeight() || m1.getHeight() != m2.getHeight()) {
            throw new InvalidDimensionException("InvalidDimensionException matrixAddition");
        }

        double[][] result = new double[m1.getHeight()][m1.getWidth()];
        for (int i = 0; i < m1.getHeight(); i++) {
            for (int j = 0; j < m1.getWidth(); j++) {
                result[i][j] = m1.getMatrixValue(i, j) - m2.getMatrixValue(i, j);
            }
        }

        IMatrix matrixAddition = new Matrix();
        matrixAddition.setMatrixValues(result);

        return matrixAddition;
    }

    /**
     * Zwraca macierz transponowaną
     *
     * @param m1
     */
    public IMatrix matrixTransposition(IMatrix m1) throws InvalidDimensionException {
        double[][] temp = new double[m1.getHeight()][m1.getWidth()];
        for (int i = 0; i < m1.getHeight(); i++) {
            for (int j = 0; j < m1.getWidth(); j++) {
                temp[j][i] = m1.getMatrixValue(i, j);
            }
        }

        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(temp);
        return matrix;
    }

    /**
     * Mnożenie macierzy przez skalar
     *
     * @param m1
     * @param scalar
     */
    public IMatrix scalarMultiplication(IMatrix m1, double scalar) {
        for (int i = 0; i < m1.getHeight(); i++) {
            for (int j = 0; j < m1.getWidth(); j++) {
                m1.setMatrixValue(i, j, m1.getMatrixValue(i, j) * scalar);
            }
        }

        return m1;
    }
}
