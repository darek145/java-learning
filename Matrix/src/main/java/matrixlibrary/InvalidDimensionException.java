package matrixlibrary;

public class InvalidDimensionException extends Exception {
    InvalidDimensionException(String message) {
        super(message);
    }
}
