package matrixlibrary;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Matrix implements IMatrix {

    public double[][] getElements() {
        return elements;
    }

    /**
     * Elements of the matrix
     */
    private double[][] elements;

    public double[][] identityMatrix;

    public void createIdentityMatrix(int size) {
        identityMatrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            identityMatrix[i][i] = 1;
        }
    }

    public double determinant() throws InvalidDimensionException {
        if (elements.length != elements[1].length) {
            throw new InvalidDimensionException("Matrix is not square!");
        }

        return tmpMatrixDeterminant(elements);
    }

    private double tmpMatrixDeterminant(double[][] matrix) {
        double temporary[][];
        double result = 0;

        if (matrix.length == 1) {
            result = matrix[0][0];
            return (result);
        }

        if (matrix.length == 2) {
            result = ((matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]));
            return (result);
        }

        for (int i = 0; i < matrix[0].length; i++) {
            temporary = new double[matrix.length - 1][matrix[0].length - 1];

            for (int j = 1; j < matrix.length; j++) {
                for (int k = 0; k < matrix[0].length; k++) {
                    if (k < i) {
                        temporary[j - 1][k] = matrix[j][k];
                    } else if (k > i) {
                        temporary[j - 1][k - 1] = matrix[j][k];
                    }
                }
            }

            result += matrix[0][i] * Math.pow(-1, (double) i) * tmpMatrixDeterminant(temporary);
        }
        return (result);
    }


    public double getMatrixValue(int row, int column) {
        return elements[row][column];
    }

    public void setMatrixValue(int row, int column, double value) {
        elements[row][column] = value;
    }

    public void setMatrixValues(double[][] values) throws InvalidDimensionException {
        if (values.length > 3 || values[1].length > 3) {
            throw new InvalidDimensionException("Matrix is not compatible!");
        }

        elements = values.clone();
    }

    public int getWidth() {
        return elements[1].length;
    }

    public int getHeight() {
        return elements.length;
    }

    public double[][] getMatrixValues() {
        return elements;
    }

    @Override
    public String toString() {
        return "Matrix{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }
}
