import matrixlibrary.IMatrix;
import matrixlibrary.InvalidDimensionException;
import matrixlibrary.Matrix;
import matrixlibrary.MatrixMath;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestMain {
    private double[][] exceptedMatrix = {{3.0, 3.0, 3.0}, {2.0, 2.0, 2.0}, {1.0, 1.0, 1.0}};

    @Test
    public void createMatrix() {
        IMatrix matrix = new Matrix();
        assertNotNull(matrix);
    }

    @Test
    public void initialMatrix() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        assertEquals(3.0, matrix.getMatrixValue(0, 0), 0);
        assertNotEquals(1.0, matrix.getMatrixValue(1, 1));
    }

    @Test
    public void setNewValue() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        matrix.setMatrixValue(0, 0, 0);

        assertEquals(0, matrix.getMatrixValue(0, 0), 0);
    }

    @Test
    public void getSize() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);

        assertEquals(3, matrix.getHeight(), 0);
        assertEquals(3, matrix.getWidth(), 0);
    }

    @Test
    public void getDeterminant() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);

        assertEquals(0.0, matrix.determinant(), 0);
    }

    @Test
    public void getIdentityMatrix() throws InvalidDimensionException {
        Matrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        matrix.createIdentityMatrix(3);

        assertEquals(1.0, matrix.identityMatrix[1][1], 0);
    }

    @Test
    public void matrixAddition() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        MatrixMath matrixMath = new MatrixMath();
        IMatrix results = matrixMath.matrixAddition(matrix, matrix);

        assertEquals(6.0, results.getMatrixValue(0, 0), 0);
    }

    @Test
    public void scalarMultiplication() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        MatrixMath matrixMath = new MatrixMath();
        IMatrix results = matrixMath.scalarMultiplication(matrix, 3.0);

        assertEquals(3.0, results.getMatrixValue(2, 2), 0);
    }

    @Test
    public void matrixSubtracting() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        MatrixMath matrixMath = new MatrixMath();
        IMatrix results = matrixMath.matrixSubtracting(matrix, matrix);

        assertEquals(0.0, results.getMatrixValue(2, 2), 0);
    }

    @Test
    public void matrixMultiplication() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        MatrixMath matrixMath = new MatrixMath();
        IMatrix results = matrixMath.matrixMultiplication(matrix, matrix);

        assertEquals(18.0, results.getMatrixValue(0, 0), 0);
    }

    @Test
    public void matrixTransposition() throws InvalidDimensionException {
        IMatrix matrix = new Matrix();
        matrix.setMatrixValues(exceptedMatrix);
        MatrixMath matrixMath = new MatrixMath();
        IMatrix results = matrixMath.matrixTransposition(matrix);

        assertEquals(3.0, results.getMatrixValue(0, 0), 0);
        assertEquals(2.0, results.getMatrixValue(0, 1), 0);
    }

}
