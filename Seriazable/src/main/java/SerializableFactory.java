import java.util.List;

class SerializableFactory {
    static Serializer getSerializer(String type, List<Object> o) {
        switch (type) {
            case "JSON":
                return new JSON(o);
            case "CSV":
                return new CSV(o);
            case "XML":
                return new XML(o);
            default:
                return null;
        }
    }
}
