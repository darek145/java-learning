import java.util.ArrayList;
import java.util.List;

public abstract class Serializer {
    String serializeValue;
    List<Object> serializeObject;

    Serializer(List<Object> serializeObject) {
        this.serializeObject = serializeObject;
    }

    public String getSerializeValue() {
        return serializeValue;
    }

    public Object getSerializeObject() {
        return serializeObject;
    }
}
