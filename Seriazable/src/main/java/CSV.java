import java.lang.reflect.Field;
import java.util.List;

public class CSV extends Serializer {
    CSV(List<Object> serializeObject) {
        super(serializeObject);
        this.serializeValue = this.parseToCSV(serializeObject);
    }

    private String parseToCSV(List<Object> objectList) {
        StringBuilder sb = new StringBuilder();
        StringBuilder allValues = new StringBuilder();
        StringBuilder headers = new StringBuilder();
        boolean setHeader = true;
        for (Object o : objectList) {
            try {
                for (Field field : o.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    Object value;
                    value = field.get(o);
                    if (value != null) {
                        if (setHeader) {
                            headers.append(field.getName()).append(";");
                        }
                        sb.append(value).append(";");
                    }
                }
                sb.append("\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            setHeader = false;
            allValues.append(sb);
        }
        return headers.append(System.getProperty("line.separator")).append(sb).toString();
    }

    public String getSerializeValue() {
        return this.serializeValue;
    }

    public Object getSerializeObject() {
        return serializeObject;
    }
}
