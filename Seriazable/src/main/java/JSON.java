import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

class JSON extends Serializer {
    JSON(List<Object> serializeObject) {
        super(serializeObject);
        this.serializeValue = this.parseToJSON(serializeObject);
    }

    private String parseToJSON(List<Object> listObjects) {
        ObjectMapper mapper = new ObjectMapper();
        StringBuilder sb = new StringBuilder();
        try {
            for (Object o : listObjects) {
                sb.append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o)).append("\n");
            }
            return sb.toString();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getSerializeValue() {
        return this.serializeValue;
    }

    public Object getSerializeObject() {
        return this.serializeObject;
    }
}
