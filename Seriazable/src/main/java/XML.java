import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.List;

class XML extends Serializer {
    private static final String FILE_NAME = "jaxb-emp.xml";
    XML(List<Object> serializeObject) {
        super(serializeObject);
        this.serializeValue = this.parseToXML(serializeObject);
    }

    private String parseToXML(List<Object> listObjects) {
        StringBuilder sb = new StringBuilder();
        for (Object o : listObjects) {
            jaxbObjectToXML(o);
        }
        return sb.toString();
    }

    private static void jaxbObjectToXML(Object emp) {

        try {
            JAXBContext context = JAXBContext.newInstance(Employee.class);
            Marshaller m = context.createMarshaller();
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Write to System.out for debugging
            m.marshal(emp, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public String getSerializeValue() {
        return this.serializeValue;
    }

    public Object getSerializeObject() {
        return serializeObject;
    }
}
