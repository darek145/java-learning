import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TestFactory {
    public static void main(String[] args) {
        TestFactory testFactory = new TestFactory();
        testFactory.runParser();
    }

    private void runParser() {
        Staff staff = createDummyObject();
        Employee emp = createEmployee();
        List<Object> staffList = new ArrayList<>();
        staffList.add(staff);
        staffList.add(staff);
        staffList.add(staff);

        List<Object> empList = new ArrayList<>();
        empList.add(emp);
        empList.add(emp);
        empList.add(emp);

        Serializer jsonSerializer = SerializableFactory.getSerializer("JSON", staffList);
        Serializer csvSerializer = SerializableFactory.getSerializer("CSV", staffList);
        Serializer xmlSerializer = SerializableFactory.getSerializer("XML", empList);

        System.out.println(Objects.requireNonNull(jsonSerializer).getSerializeValue());
        System.out.println(Objects.requireNonNull(csvSerializer).getSerializeValue());
        System.out.println(Objects.requireNonNull(xmlSerializer).getSerializeValue());
    }

    private Employee createEmployee() {
        Employee emp = new Employee();
        emp.setId(9999);
        emp.setAge(10);
        emp.setName("Jan");
        emp.setGender("Kowalski");
        emp.setRole("Malarz");
        emp.setPassword("strongPassword123");
        return emp;
    }

    private Staff createDummyObject() {

        Staff staff = new Staff();

        staff.setName("mkyong");
        staff.setAge(33);
        staff.setPosition("Developer");
        staff.setSalary(new BigDecimal("7500"));

        List<String> skills = new ArrayList<>();
        skills.add("java");
        skills.add("python");

        staff.setSkills(skills);

        return staff;
    }
}
