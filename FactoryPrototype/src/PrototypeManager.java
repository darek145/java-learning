import java.util.HashMap;
import java.util.Map;

public class PrototypeManager {
    static Map<String, PrototypeBase> prototypes = new HashMap<>();

    void addPrototype(String name, PrototypeBase prototype) {
        prototypes.put(name, prototype);
    }

    void removePrototype(String name) {
        prototypes.remove(name);
    }

    void dropAllPrototypes() {
        prototypes.clear();
    }

    public PrototypeBase getCopy(String name) {
        if (prototypes.containsKey(name)) {
            return (PrototypeBase) prototypes.get(name).Clone();
        }
        return null;
    }
}
