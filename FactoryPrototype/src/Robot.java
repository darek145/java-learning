class Robot extends PrototypeBase {
    Module type;

    @Override
    Object Clone() {
        Robot robot = new Robot();
        robot.type = type;
        return robot;
    }

    @Override
    String Action() {
        return type.getType();
    }
}
