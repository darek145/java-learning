import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Computer extends Electronics {
    private String name;
    private List<String> neededModules = new ArrayList<>();

    Computer(String name) {
        this.name = name;
        createComputer();
    }

    private void createComputer() {
        neededModules.addAll(Arrays.asList("A", "B", "C", "D"));
    }

    @Override
    public String getModules() {
        return Module.getAllModules(neededModules);
    }

    @Override
    public String getName() {
        return name;
    }
}
