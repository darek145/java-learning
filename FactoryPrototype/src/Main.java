import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        createRobots();

        Electronics TV = Electronics.ElectronicsFactory(Electronics.ElectronicsType.TV);
        Electronics COMPUTER = Electronics.ElectronicsFactory(Electronics.ElectronicsType.COMPUTER);
        Electronics TOASTER = Electronics.ElectronicsFactory(Electronics.ElectronicsType.TOASTER);

        System.out.println(Objects.requireNonNull(TV).getName() + " --> " + Objects.requireNonNull(TV).getModules());
        System.out.println(Objects.requireNonNull(COMPUTER).getName() + " --> " + Objects.requireNonNull(COMPUTER).getModules());
        System.out.println(Objects.requireNonNull(TOASTER).getName() + " --> " + Objects.requireNonNull(TOASTER).getModules());

    }

    private static void createRobots() {
        PrototypeManager manager = new PrototypeManager();

        Robot robotA = new Robot();
        robotA.type = new Module("A");
        manager.addPrototype("A", robotA);

        Robot robotB = new Robot();
        robotB.type = new Module("B");
        manager.addPrototype("B", robotB);

        Robot robotC = new Robot();
        robotC.type = new Module("C");
        manager.addPrototype("C", robotC);

        Robot robotD = new Robot();
        robotD.type = new Module("D");
        manager.addPrototype("D", robotD);
    }
}
