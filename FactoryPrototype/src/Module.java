import java.util.ArrayList;
import java.util.List;

class Module {
    private String type;

    Module(String name) {
        this.type = name;
    }

    String getType() {
        return type;
    }

    static String getAllModules(List<String> neededModules) {
        StringBuilder modulesStr = new StringBuilder();
        List<PrototypeBase> modules = new ArrayList<>();
        for (String neededModule : neededModules) {
            modules.add(PrototypeManager.prototypes.get(neededModule));
        }

        for (PrototypeBase module : modules) {
            modulesStr.append(module.Action());
        }

        return modulesStr.toString();
    }
}
