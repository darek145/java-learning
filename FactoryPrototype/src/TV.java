import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TV extends Electronics {
    private String name;
    private List<String> neededModules = new ArrayList<>();

    TV(String name) {
        this.name = name;
        createTV();
    }

    private void createTV() {
        neededModules.addAll(Arrays.asList("A", "B", "B", "B"));
    }

    @Override
    public String getModules() {
        return Module.getAllModules(neededModules);
    }

    @Override
    public String getName() {
        return name;
    }
}
