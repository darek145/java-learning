public abstract class Electronics {
    public abstract String getModules();
    public abstract String getName();

    enum ElectronicsType {
        TV,
        COMPUTER,
        TOASTER
    }

    static Electronics ElectronicsFactory(ElectronicsType electronicsType) {
        switch (electronicsType) {
            case TV: {
                return new TV("Toshiba XCZ2000");
            }
            case COMPUTER: {
                return new Computer("ASUS XYZ500");
            }
            case TOASTER: {
                return new Toaster("Toster 2000");
            }
            default:
                return null;
        }
    }
}
