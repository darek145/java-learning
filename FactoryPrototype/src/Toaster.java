import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Toaster extends Electronics {
    private String name;
    private List<String> neededModules = new ArrayList<>();

    Toaster(String name) {
        this.name = name;
        createToaster();
    }

    private void createToaster() {
        neededModules.addAll(Arrays.asList("A", "B", "D", "D"));
    }

    @Override
    public String getModules() {
        return Module.getAllModules(neededModules);
    }

    @Override
    public String getName() {
        return name;
    }
}
