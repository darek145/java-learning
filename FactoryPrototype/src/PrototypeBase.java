abstract class PrototypeBase implements Cloneable {
    abstract Object Clone();

    abstract String Action();
}
