package cellBuilder;

class NuclearMembrane {
    private String name;

    NuclearMembrane(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
