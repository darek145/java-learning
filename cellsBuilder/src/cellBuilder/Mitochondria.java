package cellBuilder;

public class Mitochondria {
    private String name;

    public Mitochondria(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
