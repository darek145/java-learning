package cellBuilder;

public class Chloroplasts {
    private String name;

    Chloroplasts(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
