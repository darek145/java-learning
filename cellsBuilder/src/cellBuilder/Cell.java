package cellBuilder;

import java.sql.SQLOutput;
import java.util.List;
import java.util.Optional;

class Cell {
    private String ARROW = " ---> ";
    private CellWall wall;
    private CellNucleus cellNucleus;
    private List<CellMembrane> cellMembranes;
    private List<EndoplasmicReticulum> endoplasmicReticules;
    private List<NuclearMembrane> nuclearMembranes;
    private List<Cytoplasm> cytoplasms;
    private List<Ribosomes> ribosomes;
    private List<Mitochondria> mitochondrias;
    private List<Vacuoles> vacuolesList;
    private List<Lysosomes> lysosomesList;
    private List<Chloroplasts> chloroplastsList;

    void setWall(CellWall wall) {
        this.wall = wall;
    }

    void setCellMembranes(List<CellMembrane> cellMembranes) {
        this.cellMembranes = cellMembranes;
    }

    void setCellNucleus(CellNucleus cellNucleus) {
        this.cellNucleus = cellNucleus;
    }

    void setEndoplasmicReticulums(List<EndoplasmicReticulum> endoplasmicReticules) {
        this.endoplasmicReticules = endoplasmicReticules;
    }

    void setNuclearMembrane(List<NuclearMembrane> nuclearMembranes) {
        this.nuclearMembranes = nuclearMembranes;
    }

    void setCytoplasms(List<Cytoplasm> cytoplasms) {
        this.cytoplasms = cytoplasms;
    }

    void setRibosomes(List<Ribosomes> ribosomes) {
        this.ribosomes = ribosomes;
    }

    void setMitochondrias(List<Mitochondria> mitochondrias) {
        this.mitochondrias = mitochondrias;
    }

    void setVacuolesList(List<Vacuoles> vacuolesList) {
        this.vacuolesList = vacuolesList;
    }

    void setLysosomesList(List<Lysosomes> lysosomesList) {
        this.lysosomesList = lysosomesList;
    }

    void setChloroplastsList(List<Chloroplasts> chloroplastsList) {
        this.chloroplastsList = chloroplastsList;
    }

    void getInfoAboutCell() {
        System.out.println("Cell has: ");
        System.out.println("Wall" + ARROW + Optional.ofNullable(wall).map(CellWall::getName).orElse("No wall"));
        System.out.println("Cell Nucleus" + ARROW + cellNucleus.getName());
        System.out.println("Cell Membrane: ");
        cellMembranes.forEach(cellMembrane -> System.out.println(cellMembrane.getName()));
        endoplasmicReticules.forEach(endoplasmicReticulum -> System.out.println(endoplasmicReticulum.getName()));
        nuclearMembranes.forEach(nuclearMembrane -> System.out.println(nuclearMembrane.getName()));
        cytoplasms.forEach(cytoplasm -> System.out.println(cytoplasm.getName()));
        ribosomes.forEach(ribosomes -> System.out.println(ribosomes.getName()));
        mitochondrias.forEach(mitochondria -> System.out.println(mitochondria.getName()));
        vacuolesList.forEach(vacuoles -> System.out.println(vacuoles.getName()));
        lysosomesList.forEach(lysosomes -> System.out.println(lysosomes.getName()));
        chloroplastsList.forEach(chloroplasts -> System.out.println(chloroplasts.getName()));
    }
}