package cellBuilder;

public class Cytoplasm {
    private String name;

    public Cytoplasm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
