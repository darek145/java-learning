package cellBuilder;

import java.util.List;

abstract class CellBuilder {
    Cell cell;

    Cell getCell() {
        return cell;
    }

    void createNewCell() {
        cell = new Cell();
    }

    abstract void buildCellWall();

    abstract void buildCellNucleus();

    abstract void buildCellMembrane(List<CellMembrane> cellMembranes);

    abstract void buildEndoplasmicReticulum(List<EndoplasmicReticulum> endoplasmicReticules);

    abstract void buildNuclearMembranes(List<NuclearMembrane> nuclearMembranes);

    abstract void buildCytoplasms(List<Cytoplasm> cytoplasms);

    abstract void buildRibosomes(List<Ribosomes> ribosomes);

    abstract void buildMitochondrias(List<Mitochondria> mitochondrias);

    abstract void buildVacuoles(List<Vacuoles> vacuoles);

    abstract void buildLysosomes(List<Lysosomes> lysosomesList);

    abstract void buildChloroplasts(List<Chloroplasts> chloroplastsList);
}
