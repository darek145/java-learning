package cellBuilder;

import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        CellDirector cellDirector = new CellDirector();
        CellBuilder animalCell = new AnimalCell();

        cellDirector.setCellBuilder(animalCell);
        cellDirector.cellCreator(
                Collections.singletonList(new CellMembrane("cell-membrane-1")),
                Collections.singletonList(new EndoplasmicReticulum("EndoplasmicReticulum")),
                Collections.singletonList(new NuclearMembrane("nuclear-membrane")),
                Collections.singletonList(new Cytoplasm("cytoplasmes")),
                Collections.singletonList(new Ribosomes("ribosomes")),
                Collections.singletonList(new Mitochondria("mitochondira")),
                Collections.singletonList(new Vacuoles("vacuoles")),
                Collections.singletonList(new Lysosomes("lysosomes")),
                Collections.emptyList());

        Cell cell = cellDirector.getCell();
        System.out.println("Animal");
        cell.getInfoAboutCell();

        CellDirector cellDirector1 = new CellDirector();
        CellBuilder plantCell = new CellPlant();

        cellDirector1.setCellBuilder(plantCell);
        cellDirector1.cellCreator(
                Collections.singletonList(new CellMembrane("cell-membrane-1")),
                Collections.singletonList(new EndoplasmicReticulum("EndoplasmicReticulum")),
                Collections.singletonList(new NuclearMembrane("nuclear-membrane")),
                Collections.singletonList(new Cytoplasm("cytoplasmes")),
                Collections.singletonList(new Ribosomes("ribosomes")),
                Collections.singletonList(new Mitochondria("mitochondira")),
                Collections.singletonList(new Vacuoles("vacuoles")),
                Collections.singletonList(new Lysosomes("lysosomes")),
                Collections.singletonList(new Chloroplasts("chloroplasts")));

        Cell cellPlant = cellDirector1.getCell();
        System.out.println();
        System.out.println("Plant");
        cellPlant.getInfoAboutCell();
    }
}
