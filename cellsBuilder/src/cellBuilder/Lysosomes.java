package cellBuilder;

public class Lysosomes {
    private String name;

    public Lysosomes(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
