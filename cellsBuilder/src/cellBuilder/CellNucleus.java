package cellBuilder;

public class CellNucleus {
    private String name;

    CellNucleus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
