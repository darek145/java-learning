package cellBuilder;

class CellMembrane {
    private String name;

    CellMembrane(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
