package cellBuilder;

import java.util.List;

class CellDirector {
    private CellBuilder cellBuilder;

    void setCellBuilder(CellBuilder cellBuilder) {
        this.cellBuilder = cellBuilder;
    }

    Cell getCell() {
        return cellBuilder.getCell();
    }

    void cellCreator(List<CellMembrane> cellMembranes,
                     List<EndoplasmicReticulum> endoplasmicReticules,
                     List<NuclearMembrane> nuclearMembranes,
                     List<Cytoplasm> cytoplasms,
                     List<Ribosomes> ribosomes,
                     List<Mitochondria> mitochondrias,
                     List<Vacuoles> vacuolesList,
                     List<Lysosomes> lysosomesList,
                     List<Chloroplasts> chloroplastsList) {
        cellBuilder.createNewCell();
        cellBuilder.buildCellWall();
        cellBuilder.buildCellNucleus();
        cellBuilder.buildCellMembrane(cellMembranes);
        cellBuilder.buildEndoplasmicReticulum(endoplasmicReticules);
        cellBuilder.buildNuclearMembranes(nuclearMembranes);
        cellBuilder.buildCytoplasms(cytoplasms);
        cellBuilder.buildRibosomes(ribosomes);
        cellBuilder.buildMitochondrias(mitochondrias);
        cellBuilder.buildVacuoles(vacuolesList);
        cellBuilder.buildLysosomes(lysosomesList);
        cellBuilder.buildChloroplasts(chloroplastsList);
    }
}
