package cellBuilder;

class CellWall {
    private String name;

    CellWall(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
