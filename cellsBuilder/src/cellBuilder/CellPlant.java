package cellBuilder;

import java.util.List;

class CellPlant extends CellBuilder {
    @Override
    public void buildCellWall() {
        cell.setWall(new CellWall("cell-wall"));
    }

    @Override
    void buildCellNucleus() {
        cell.setCellNucleus(new CellNucleus("cell-nucleus"));
    }

    @Override
    void buildCellMembrane(List<CellMembrane> cellMembranes) {
        cell.setCellMembranes(cellMembranes);
    }

    @Override
    void buildEndoplasmicReticulum(List<EndoplasmicReticulum> endoplasmicReticules) {
        cell.setEndoplasmicReticulums(endoplasmicReticules);
    }

    @Override
    void buildNuclearMembranes(List<NuclearMembrane> nuclearMembranes) {
        cell.setNuclearMembrane(nuclearMembranes);
    }

    @Override
    void buildCytoplasms(List<Cytoplasm> cytoplasms) {
        cell.setCytoplasms(cytoplasms);
    }

    @Override
    void buildRibosomes(List<Ribosomes> ribosomes) {
        cell.setRibosomes(ribosomes);
    }

    @Override
    void buildMitochondrias(List<Mitochondria> mitochondrias) {
        cell.setMitochondrias(mitochondrias);
    }

    @Override
    void buildVacuoles(List<Vacuoles> vacuoles) {
        cell.setVacuolesList(vacuoles);
    }

    @Override
    void buildLysosomes(List<Lysosomes> lysosomesList) {
        cell.setLysosomesList(lysosomesList);
    }

    @Override
    void buildChloroplasts(List<Chloroplasts> chloroplastsList) {
        cell.setChloroplastsList(chloroplastsList);
    }
}
