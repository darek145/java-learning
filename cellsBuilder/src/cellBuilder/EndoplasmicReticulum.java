package cellBuilder;

public class EndoplasmicReticulum {
    private String name;

    public EndoplasmicReticulum(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
